**Please Note** : The objectives are not completed. POP up form is not implemented.

**Some troubles**

1. Facebook login button action was not completely tested since they were constantly blocking the attempts.

2. / route (dashboard) login check could not be tested properly due to (1)

3. Dashboard was directly copied from the provided reference, **it was not written from scratch**.

There were some issues with local testing even after configuring `fb developer -> project` with `localhost`. So a copy is hosted at https://doccuretesting.herokuapp.com/

### Updates
I have done the changes specified in your response mail and now, facebook login is working correctly. But I am sorry to say that I failed to initialize facebook sdk at most once; when ever I tried, errors were thrown : `window.FB` is undefined or `FB` is undefined. 

Thank you.