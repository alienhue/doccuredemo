import React from 'react'
import Axios from 'axios'
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import css from './styles.module.css'

class AddToken extends React.Component{
    state = {
        generalStatusMessage : '',
        departmentNames : [],
        //authToken
        authToken : '',
        //token details from user
        name : '',
        age : '',
        gender : '',
        selectedDepartment : ''
    }

    componentDidMount(){
        var authToken = localStorage.getItem('authToken')
        if(authToken === null){
            console.log("+ No token")
            window.location = "../login"
        }
        else{
            console.log("- No token")
            let params = {
                "token" : 'Bearer ' + authToken,
            }
            Axios.post("https://token-plus.herokuapp.com/varsify",params).then(res=>{
                if(res.data.status !== 0) window.location = "../login"
                else{
                    Axios.post("https://token-plus.herokuapp.com/getDepartmentDetailsActive").then(res=>{
                        console.log(res.data)
                        if(res.data.status === 0){
                            var deptNames = []
                            res.data.departments.forEach(dept=>{
                                if(!deptNames.includes(dept.department))
                                    deptNames.push(dept.department)
                            })
                            this.setState({departmentNames : deptNames, authToken : authToken})
                        }
                        else throw Error("Error")
                    }).catch(err=>{
                        this.setState({generalStatusMessage : "Could not fetch data. Please try again by refreshing the page."})
                    })
                }
            }).catch(err=>{console.log(err)})
        }
    }

    handleChange = (e) =>{
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    handleSubmit = (e)=>{
        e.preventDefault()
        if(this.state.selectedDepartment === '' || this.state.age === '' || this.state.name === '' || this.state.gender === ''){
            this.setState({generalStatusMessage : "Fill all the fields"})
            return
        }
        var answer = window.confirm("Are you sure?")
        if(answer){
            this.setState({generalStatusMessage : "Working on it.."})
            let params = {
                "token" : this.state.authToken,
                "name" : this.state.name,
                "age" : parseInt(this.state.age),
                "gender" : this.state.gender,
                "department" : this.state.selectedDepartment,
                "from" : "hosp-pinappescmefompne-saarlisjck"
            }
            Axios.post("https://token-plus.herokuapp.com/regToken",params).then(res=>{
                console.log(res.data)
                if(res.data.status === 0){
                    this.setState({generalStatusMessage : "Added Successfully", name : '', age : ''}, )
                }
                else if(res.data.status === 2){
                    this.setState({generalStatusMessage : res.data.desc})
                }
                else throw Error("Error")
            }).catch(err=>{
                this.setState({generalStatusMessage : "Could not fetch data. Please try again by refreshing the page."})
            })
        }
    }
    
    render(){
        return(
            <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header className="mdl-layout__header">
                <div className="mdl-layout__header-row">
                <span className="mdl-layout-title">Dashboard</span>
                <div className="mdl-layout-spacer"></div>
                <nav className="mdl-navigation mdl-layout--large-screen-only">
                    <a className="mdl-navigation__link" href="./editDept">Edit Department Data</a>
                    <a className="mdl-navigation__link" href="../admin">Home</a>
                </nav>
                </div>
            </header>
            <div className="mdl-layout__drawer">
                <span className="mdl-layout-title">Dashboard</span>
                <nav className="mdl-navigation">
                    <a className="mdl-navigation__link" href="./editDept">Edit Department Data</a>
                    <a className="mdl-navigation__link" href="../admin">Home</a>
                </nav>
            </div>
            <main className="mdl-layout__content">
            <center>
                <div className={css.loginCard}>
                <div className="page-content">
                    <div className="demo-card-wide mdl-card mdl-shadow--2dp" style={{width:'100%',height:'auto'}}>
                        <div style={{padding:20}}>
                            <span className="mdl-chip mdl-chip--contact">
                                <span className="mdl-chip__contact mdl-color--teal mdl-color-text--white">+</span>
                                <span className="mdl-chip__text">Add Token</span>
                            </span>
                            <div>
                                {
                                    this.state.generalStatusMessage !== '' ? <div style={{color : '#000', marginTop : 40, background : '#ffcc80', padding : 12}}>{this.state.generalStatusMessage}</div> : null
                                }
                                <form action="#"onSubmit={this.handleSubmit}>
                                    <br/><br/>
                                    {
                                        this.state.departmentNames.length === 0 ? null : 
                                        <Dropdown options={this.state.departmentNames} onChange={(e)=>{this.setState({selectedDepartment : e.value})}} value={this.state.selectedDepartment} placeholder="Department"/>
                                    }
                                    <br/>
                                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input className="mdl-textfield__input" type="text" id="name" value={this.state.name} onChange={this.handleChange}/>
                                        <label className="mdl-textfield__label" htmlFor="name">Patient Name</label>
                                    </div>
                                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input className="mdl-textfield__input" type="number" id="age" value={this.state.age}  onChange={this.handleChange}/>
                                        <label className="mdl-textfield__label" htmlFor="age">Age</label>
                                    </div>
                                    {
                                        <Dropdown options={['Male','Female','Other']} onChange={(e)=>{this.setState({gender : e.value})}} value={this.state.gender} placeholder="Gender" />
                                    }
                                    <br/>
                                    <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onClick={this.handleSubmit}>
                                    Add Token
                                    </button>
                                    <br/><br/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </center>
            </main>
            </div>
        )
    }
}

export default AddToken