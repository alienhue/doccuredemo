import React from 'react'
import Axios from 'axios'
import css from './styles.module.css'

class EditDept extends React.Component{
    state = {
        generalStatusMessage : '',
        //department details
        departments : [],
        //authToken
        authToken : ''
    }

    componentDidMount(){
        var authToken = localStorage.getItem('authToken')
        if(authToken === null){
            console.log("+ No token")
            window.location = "../login"
        }
        else{
            console.log("- No token")
            let params = {
                "token" : 'Bearer ' + authToken,
            }
            Axios.post("https://token-plus.herokuapp.com/varsify",params).then(res=>{
                if(res.data.status !== 0) window.location = "../login"
                else{
                    Axios.post("https://token-plus.herokuapp.com/getDepartmentDetails").then(res=>{
                        console.log(res.data)
                        if(res.data.status === 0){
                            this.setState({departments : res.data.departments, authToken : authToken})
                        }
                        else throw Error("Error")
                    }).catch(err=>{
                        this.setState({generalStatusMessage : "Could not fetch data. Please try again by refreshing the page."})
                    })
                }
            }).catch(err=>{console.log(err)})
        }
    }

    handleChange = (e) =>{
        var index = e.target.id
        var depts = this.state.departments
        depts[index].totTokens = e.target.value
        this.setState({departments : depts})
    }

    changeActivityStatus = (index) => {
        var dept = this.state.departments[index]
        var status = dept.status
        var depts = this.state.departments
        depts[index].status = status === 1 ? 0 : 1
        this.setState({departments : depts})
    }

    handleSubmit = (e)=>{
        e.preventDefault()
    }

    updateData = (index) => {
        var answer = window.confirm("Are you sure?")
        if(answer){
            this.setState({generalStatusMessage : "Working on it.."})
            let params = {
                "token" : this.state.authToken,
                "deptID" : this.state.departments[index]._id,
                "totTokens" : this.state.departments[index].totTokens,
                "status" : parseInt(this.state.departments[index].status)
            }
            Axios.post("https://token-plus.herokuapp.com/updateSchedule",params).then(res=>{
                console.log(res.data)
                if(res.data.status === 0){
                    this.setState({generalStatusMessage : "Updated Successfully"})
                    alert("Updated Successfully")
                }
                else if(res.data.status === 2){
                    this.setState({generalStatusMessage : res.data.desc})
                }
                else throw Error("Error")
            }).catch(err=>{
                this.setState({generalStatusMessage : "Could not fetch data. Please try again by refreshing the page."})
            })
        }
    }
    
    render(){
        return(
            <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header className="mdl-layout__header">
                <div className="mdl-layout__header-row">
                <span className="mdl-layout-title">Dashboard</span>
                <div className="mdl-layout-spacer"></div>
                <nav className="mdl-navigation mdl-layout--large-screen-only">
                    <a className="mdl-navigation__link" href="../admin">Home</a>
                    <a className="mdl-navigation__link" href="./addToken">Add Token</a>
                </nav>
                </div>
            </header>
            <div className="mdl-layout__drawer">
                <span className="mdl-layout-title">Dashboard</span>
                <nav className="mdl-navigation">
                    <a className="mdl-navigation__link" href="../admin">Home</a>
                    <a className="mdl-navigation__link" href="./addToken">Add Token</a>
                </nav>
            </div>
            <main className="mdl-layout__content">
            <center>
                <div className={css.editDeptCard}>
                <div className="page-content">
                    <div className="demo-card-wide mdl-card mdl-shadow--2dp" style={{width:'100%',height:'auto'}}>
                        <div style={{padding:20}}>
                            <span className="mdl-chip mdl-chip--contact">
                                <span className="mdl-chip__contact mdl-color--teal mdl-color-text--white">&#9888;</span>
                                <span className="mdl-chip__text">Department Details</span>
                            </span>
                            <div>
                                {
                                    this.state.generalStatusMessage !== '' ? <div style={{color : '#000', marginTop : 40, marginBottom : 40, background : '#ffcc80', padding : 12}}>{this.state.generalStatusMessage}</div> : null
                                }
                                <form action="#" onSubmit={this.handleSubmit}>
                                <div>
                                    {
                                        this.state.departments.map((dept,index)=>{
                                            return(
                                                <div key = {dept._id} style={{marginBottom : 20}}>
                                                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        {dept.department}
                                                    </div>
                                                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style={{background:'#efefef', paddingLeft : 20, width : 170, marginRight : 30, borderRadius : 3}}>
                                                        {
                                                            dept.status === 1 ? <span style={{color:'#00c853'}}>Active&nbsp;&nbsp;</span> : <span style={{color:'#d50000'}}>Inactive</span>
                                                        }
                                                        <button style={{color : '#4285f4', padding:10, marginLeft : 10,  border : 0, boxShadow : '1px 2px 6px rgba(0,0,0,0.2)'}} onClick = {()=>{this.changeActivityStatus(index)}}>Change</button>
                                                    </div>
                                                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style={{width:60}}>
                                                        <input className="mdl-textfield__input" type="number" placeholder={dept.defaultTokenCount} id={index}  value={dept.totTokens} onChange={this.handleChange}/>
                                                    </div>
                                                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style={{width:60}}>
                                                        <button style={{color : '#fff', background : '#3949ab', padding:10, marginLeft : 10,  border : 0}} onClick = {()=>{this.updateData(index)}}>UPDATE</button>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </center>
            </main>
            </div>
        )
    }
}

export default EditDept