import React from 'react'
import Axios from 'axios'
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import css from './styles.module.css'

//accessory fn to order tokens by time
function compare( a, b ) {
    if ( a.time < b.time ){
      return -1;
    }
    if ( a.time > b.time ){
      return 1;
    }
    return 0;
}

class AdminDash extends React.Component{
    state = {
        generalStatusMessage : '',
        tokens : [],
        departmentNames : [],
        selectedDepartment : '',
        tokenCountPerDept : {}
    }

    componentDidMount(){
        var authToken = localStorage.getItem('authToken')
        if(authToken === null){
            console.log("+ No token")
            window.location = "../login"
        }
        else{
            console.log("- No token")
            let params = {
                "token" : 'Bearer ' + authToken,
            }
            Axios.post("https://token-plus.herokuapp.com/varsify",params).then(res=>{
                if(res.data.status !== 0) window.location = "../login"
                else{
                    Axios.post("https://token-plus.herokuapp.com/todaysToken",{token : authToken}).then(res=>{
                        console.log(res.data)
                        if(res.data.status === 0){
                            //sort based on time slot
                            res.data.tokens.sort( compare );
                            var deptNames = []
                            var countPerDept = {}
                            res.data.tokens.forEach(token=>{
                                if(!deptNames.includes(token.departmentName))
                                    deptNames.push(token.departmentName)
                                if(token.departmentName in countPerDept){
                                    countPerDept[token.departmentName] += 1
                                }
                                else{
                                    countPerDept[token.departmentName] = 1
                                }
                            })
                            deptNames.sort()
                            this.setState({departments : res.data.departments, tokens : res.data.tokens, departmentNames : deptNames, selectedDepartment : deptNames[0], tokenCountPerDept : countPerDept })
                        }
                        else throw Error("Error")
                    }).catch(err=>{
                        this.setState({generalStatusMessage : "Could not fetch data. Please try again by refreshing the page."})
                    })
                }
            }).catch(err=>{console.log(err)})
        }
    }

    downloadTokenData = function(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '-' + dd + '-' + yyyy;
        var stringToWrite = `Date    : ${today}\nColumns : TokenNum - Time of Appointment - Age - Gender - Name\n\n`
        Object.keys(this.state.tokenCountPerDept).map((deptName,index)=>{
            stringToWrite+=(deptName + "\n\n")
            var age, gender,tokenNum;
            this.state.tokens.map((token,index)=>{
                if(token.departmentName === deptName){
                    tokenNum =  token.tokenNumber < 10 ? "0" + token.tokenNumber : token.tokenNumber
                    age = token.age < 10 ? age = "0" + token.age : token.age
                    if(token.gender === 'Male') gender = 'M';
                    else if(token.gender === 'Female') gender = 'F';
                    else gender = 'O';
                    stringToWrite+=(tokenNum + "    " +
                        token.time.slice(0,2) + ":" + token.time.slice(2) + "    " + 
                        age + "    " + gender + "    " + token.name + "   \n"
                    )
                }
            })
            stringToWrite+=("\n\n")
        })
        
        var fileName = today + '-tokens-data-file';
        const element = document.createElement("a");
        const file = new Blob([stringToWrite], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = fileName + ".txt";
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    }

    
    render(){
        return(
            <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header className="mdl-layout__header">
                <div className="mdl-layout__header-row">
                <span className="mdl-layout-title">Dashboard</span>
                <div className="mdl-layout-spacer"></div>
                <nav className="mdl-navigation mdl-layout--large-screen-only">
                    <a className="mdl-navigation__link" href="./editDept">Edit Department Data</a>
                    <a className="mdl-navigation__link" href="./addToken">Add Token</a>
                </nav>
                </div>
            </header>
            <div className="mdl-layout__drawer">
                <span className="mdl-layout-title">Dashboard</span>
                <nav className="mdl-navigation">
                    <a className="mdl-navigation__link" href="./editDept">Edit Department Data</a>
                    <a className="mdl-navigation__link" href="./addToken">Add Token</a>
                </nav>
            </div>
            <main className="mdl-layout__content">
            <center>
                <div className={css.adminCard}>
                <div className="page-content">
                    <div className="demo-card-wide mdl-card mdl-shadow--2dp" style={{width:'100%',height:'auto'}}>
                        <div style={{padding:20}}>
                            <span className="mdl-chip mdl-chip--contact">
                                <span className="mdl-chip__contact mdl-color--teal mdl-color-text--white">&#10003;</span>
                                <span className="mdl-chip__text">Today's Tokens</span>
                            </span>
                            <div>
                                <br/>
                                <button onClick={()=>this.downloadTokenData()}>Download Data</button>
                            </div>
                            <div>
                                {
                                    this.state.tokenCountPerDept.length === 0 ? null : 
                                    <table className="mdl-data-table mdl-js-data-table" style = {{marginTop : 20, width :'100%', boxShadow : '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)', background : '#eceff1', border:0}}>
                                        <thead>
                                            <tr>
                                                <th className="mdl-data-table__cell--non-numeric">Department</th>
                                                <th>Total Tokens</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {   
                                            Object.keys(this.state.tokenCountPerDept).map((deptName,index)=>{
                                                return(
                                                    <tr key = {deptName}>
                                                        <td className="mdl-data-table__cell--non-numeric">{deptName}</td>
                                                        <td>{this.state.tokenCountPerDept[deptName]}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                }
                                <br/>
                                {
                                    this.state.tokens.length === 0 ? <div>No tokens booked yet</div> : 
                                    <Dropdown options={this.state.departmentNames} onChange={(e)=>{this.setState({selectedDepartment : e.value})}} value={this.state.selectedDepartment} placeholder="Select an option" />
                                }
                                <div>
                                    <table className="mdl-data-table mdl-js-data-table" style = {{marginTop : 20, width :'100%'}}>
                                        <thead>
                                            <tr>
                                                <th className="mdl-data-table__cell--non-numeric">ReferenceID</th>
                                                <th className="mdl-data-table__cell--non-numeric">No.</th>
                                                <th className="mdl-data-table__cell--non-numeric">Time Slot</th>
                                                <th className="mdl-data-table__cell--non-numeric">Name</th>
                                                <th className="mdl-data-table__cell--non-numeric">Gender</th>
                                                <th>Age</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {   
                                            this.state.tokens.map((token,index)=>{
                                                if(token.departmentName === this.state.selectedDepartment)
                                                    return(
                                                        <tr key = {index}>
                                                            <td className="mdl-data-table__cell--non-numeric">{token._id.substr(19).toUpperCase()}</td>
                                                            <td className="mdl-data-table__cell--non-numeric">{token.tokenNumber}</td>
                                                            <td className="mdl-data-table__cell--non-numeric">{token.time.slice(0,2) + ":" + token.time.slice(2)}</td>
                                                            <td className="mdl-data-table__cell--non-numeric">{token.name}</td>
                                                            <td className="mdl-data-table__cell--non-numeric">{token.gender }</td>
                                                            <td>{token.age}</td>
                                                        </tr>
                                                    )
                                                else return null
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </center>
            </main>
            </div>
        )
    }
}

export default AdminDash