import React, { Component} from 'react';
import {Redirect} from 'react-router-dom'
import LoginScreen from '../components/LoginScreen'
import {faceBookConfig} from '../components/constants/facebookConfig'
import '../components/loginScreen.css'
 
export default class Login extends Component {

    constructor(props){
        super(props)
        this.state = {
            checking : true,
            loggedIn : false
        }
    }

    componentDidMount(){
        const that = this
        window.fbAsyncInit = function() {
            window.FB.init(faceBookConfig);
            that.checkLoginStatus()
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    checkLoginStatus = () => {
        const that  = this
        window.FB.getLoginStatus(function(response) {
            console.log(response)
            if (response.status === 'connected') {
                console.log("LoggedIN")
                that.setState({checking : false, loggedIn : true})
            } 
            else{
            console.log("Not loggedIN")
            that.setState({checking : false, loggedIn : false})
            }
        })
    }
 
    render() {
        return (
            <div className = {"mainContainer"}>
                {
                    this.state.checking ? 
                    <LoginScreen/>
                    :
                    this.state.loggedIn  ? 
                        //move to dashboard
                        <Redirect to="./" /> : 
                        <LoginScreen/>
                }
            </div>
        );
    }
}