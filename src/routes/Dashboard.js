import React, { Component } from 'react'
import Skeleton from 'react-loading-skeleton'
import DashboardScreen from '../components/DashboardScreen'
import '../components/loginScreen.css'

export class Dashboard extends Component {
    constructor(){
        super()
        this.state = {
            checking : true,
            loggedIn : false
        }
    }

    componentDidMount(){
        const that = this
        window.fbAsyncInit = function() {
            window.FB.init({
                appId      : "287168162603646",
                cookie     : true,
                xfbml      : true,
                version    : 'v7.0'
            });

            that.checkLoginStatus()
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    setStateFb = (response) => {
        if (response.status === 'connected') {
            console.log("LoggedIN")
            this.setState(
                {checking : false, loggedIn : true},
                ()=>{
                    const requiredFields = ['first_name','last_name', 'email', 'birthday','hometown',/*others requires*/]
                    window.FB.api('/me',{fields: requiredFields}, function(response){
                    console.log(response); 
                    //for use in `edit option` in dashboard
                    localStorage.setItem('userDetails', JSON.stringify(response))
                });
            })
        } 
        else{
            console.log("Not loggedIN")
            this.setState({checking : false, loggedIn : false})
        }
    }

    checkLoginStatus = () => {
        const that  = this
        window.FB.getLoginStatus(function(response) {
            that.setStateFb(response)
        })
    }

    BackToLogin = () => {
        window.location = "./login"
    }   
    
    render() {
        return (
            this.state.checking ? 
            <div style={{overflow : 'hidden'}}>
                <Skeleton height={1000} width={10000} />
            </div>
            :
            !this.state.loggedIn ? 
                <this.BackToLogin/>
            :
            <DashboardScreen/>
        )
    }
}

export default Dashboard
