import React, { Component } from 'react'
import './loginScreen.css'

const LoginWithUserNameAndPassword = (props) => {
    return (
        <div className={"login-form"}>
            <input className={"login-form-input"} placeholder={"Email"}/>
            <input className={"login-form-input"} placeholder={"Password"}/>
            <button onClick={()=>{props.clickAction()}} className={"login-form-button"}>
                Login
            </button>
            <a href="#" onClick={()=>{props.clickAction()}}>Forgot Password?</a>
        </div>
    )
}

export default LoginWithUserNameAndPassword
