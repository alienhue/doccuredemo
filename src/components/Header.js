import React from 'react'
import 'font-awesome/css/font-awesome.min.css';
import Logout from './Logout'

const Header = () => {
    let menuDisplayState = false
	const showMenu = () => {
        const menuToggle = document.getElementById("menuToggle")
	    const menuItem = document.getElementById("menuItem")
		menuDisplayState = !menuDisplayState
		if(menuDisplayState){
			menuToggle.className="nav-item dropdown has-arrow"
			menuItem.className="dropdown-menu"
		}
		else{
			menuToggle.className="nav-item dropdown has-arrow show"
			menuItem.className="dropdown-menu show"
		}
	}
    return (
        <div className="header">
            <div className="header-left">
                <a href="https://dreamguys.co.in/demo/doccure/template/admin/index.html" className="logo">
                    <img src="https://dreamguys.co.in/demo/doccure/template/admin/assets/img/logo.png" alt="Logo" />
                </a>
                <a href="https://dreamguys.co.in/demo/doccure/template/admin/index.html" className="logo logo-small">
                    <img src="https://dreamguys.co.in/demo/doccure/template/admin/assets/img/logo-small.png" alt="Logo" width="30" height="30" />
                </a>
            </div>
            
            <div className="top-nav-search">
                <form>
                    <input type="text" className="form-control" placeholder="Search here" />
                    <button className="btn" type="submit"><i className="fa fa-search"></i></button>
                </form>
            </div>

            <a className="mobile_btn" id="mobile_btn">
                <i className="fa fa-bars"></i>
            </a>

            <ul className="nav user-menu" onClick={()=>showMenu()}>
                <li className="nav-item dropdown has-arrow" id="menuToggle">
                    <a className="dropdown-toggle nav-link" data-toggle="dropdown">
                        <span className="user-img"><img className="rounded-circle" src="https://dreamguys.co.in/demo/doccure/template/admin/assets/img/profiles/avatar-01.jpg" alt="Ryan Taylor" width="31"/></span>
                    </a>
                    <div className="dropdown-menu" id="menuItem">
                        <Logout/>
                    </div>
                </li>
            </ul>
        </div>
    )
}

export default Header
