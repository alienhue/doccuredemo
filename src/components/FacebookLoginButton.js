import React from 'react';
import {faceBookConfig} from './constants/facebookConfig'
import 'font-awesome/css/font-awesome.min.css';

class FacebookLoginButton extends React.Component {
    constructor(){
      super()
      this.state = {
        loggedIn : false
      }
    }
    componentDidMount() {
      window.fbAsyncInit = function() {
        window.FB.init(faceBookConfig);
      };
      (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    }

    checkLoginState() {
      window.FB.getLoginStatus(function(response) {
        console.log(response)
        if(response.status === 'connected'){
          window.location = "./"
        }
      }.bind(this));
    }

    doLogin() {
        window.FB.login(()=>this.checkLoginState());
    }

    faceBookLogo = () =>{
      return <i className="fa fa-facebook-official fa-2x" aria-hidden="true" style={{color:"#3b5998"}}></i>
    }

    render() {
        return (
            <button onClick = {()=>this.doLogin()} className={"social-login-button"}><this.faceBookLogo/></button>
        );
    }
}

export default FacebookLoginButton;