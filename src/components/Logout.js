import React,{useEffect} from 'react'
import {faceBookConfig} from './constants/facebookConfig'

function Logout() {
    useEffect(()=>{
        window.fbAsyncInit = function() {
            window.FB.init(faceBookConfig);
        };
        
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    },[])

    const logout = () =>{
        if(window.FB){
            window.FB.logout(function(response) {
                console.log(response)
                window.location = "./login"
            })
        }
    }

    return (
        <a href="#" className="dropdown-item" onClick={()=>{logout()}}>Logout</a>
    )
}

export default Logout
