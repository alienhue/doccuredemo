import React from 'react'
import './loginScreen.css'

function Divider() {
    return (
        <div className={"divider-line"}>
            <div className={"divider-overlay-content"}>OR</div>
        </div>
    )
}

export default Divider
