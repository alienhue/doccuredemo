import React from 'react'
import Header from './Header'
import ProfilePage from './ProfilePage'
import UsersPage from './UsersPage'
import './bootstrap.css'
import './feathericon.css'
import './dashboard.css'

export class DashboardScreen extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			selected : 'profile',
			screens : {
				'profile' : <ProfilePage/>,
				'users' : <UsersPage/>
			}
		}
	}

	choosePage = (page) => {
		console.log(page)
		this.setState({
			selected : page
		},()=>{
			console.log(this.state.selected)
		})
	}

	render() {
		return (
			<div className="main-wrapper">
				<Header/>
				<div className="sidebar" id="sidebar">
					<div className="slimScrollDiv" style={{position : 'relative', overflow : 'hidden', width : '100%', height: '616px'}}><div className="sidebar-inner slimscroll" style={{overflow : 'hidden', width : '100%', height : '616px'}}>
						<div id="sidebar-menu" className="sidebar-menu">
							<ul>
								<li className="menu-title"> 
									<span>Pages</span>
								</li>
								{
									['profile', 'users'].map(item=>{
										return (
											<li className= {this.state.selected === item ? "active" : ""}
												onClick={()=>{this.choosePage(item)}} key={item}> 
												<a><i className="fa fa-user"></i> <span>{item}</span></a>
											</li>
										)
									})
								}
							</ul>
						</div>
					</div>
					</div>
				</div>
				<div className="page-wrapper" style={{minHeight : '676px'}}>
					{
						this.state.screens[this.state.selected]
					}
				</div>
			
			</div>
		)
	}
}

export default DashboardScreen
