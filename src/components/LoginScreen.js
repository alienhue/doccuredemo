import React, { Component } from 'react'
import {ToastsContainer, ToastsStore, ToastsContainerPosition} from 'react-toasts';
import '../components/loginScreen.css'
import LoginWithUserNameAndPassword from '../components/LoginWithUserNameAndPassword'
import FacebookLoginButton from './FacebookLoginButton'
import GoogleLoginButton from './GoogleLoginButton'
import Divider from './Divider'


export class LoginScreen extends Component {
    showToast = () => {
        ToastsStore.success("Other modes of login are currently in development. Please click the facebook icon to login to your account through facebook")
    }

    render() {
        return (
            <div>
                {/* <button onClick={()=>this.doLogin()}>Login</button> */}
                <div className={"left"}>
                    <img src="https://dreamguys.co.in/demo/doccure/template/admin/assets/img/logo-white.png"/>
                </div>
                <div className={"right"}>
                    <div className = {"right-padding"}>
                        <div className={"right-heading"}>Login</div>
                        <div className={"right-subheading"}>Access to our dashboard</div>
                        <LoginWithUserNameAndPassword clickAction={() => this.showToast()}/>
                        <Divider/>
                        <div className={"sociallogin-container"}>
                            <FacebookLoginButton />
                            <GoogleLoginButton clickAction={() => this.showToast()}/>
                        </div>
                        <div className={"register-container"}>
                            Don’t have an account? <a href="#" onClick={() => this.showToast()}><span className={"highlited-text"}>Register</span></a>
                        </div>
                        <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.BOTTOM_CENTER} lightBackground/>
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginScreen
