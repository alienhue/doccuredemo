import React from 'react'
import 'font-awesome/css/font-awesome.min.css';

function GoogleLoginButton(props) {
    return (
        <button onClick = {()=>{props.clickAction()}} className={"social-login-button"}>
            <i className="fa fa-google fa-2x" aria-hidden="true" style={{color:'#db3236'}}></i>
        </button>
    )
}

export default GoogleLoginButton
